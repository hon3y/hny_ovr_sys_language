TCEFORM.sys_language {
    language_isocode.addItems.de-at = German (Austria)
    language_isocode.addItems.de-ch = German (Switzerland)
    language_isocode.addItems.en-au = English (Australia)
    language_isocode.addItems.en-ca = English (Canada)
    language_isocode.addItems.en-gb = English (Great Britain)
    language_isocode.addItems.en-uk = English (United Kingdom)
    language_isocode.addItems.en-us = English (USA)
    language_isocode.addItems.fr-be = French (Belgium)
    language_isocode.addItems.fr-ca = French (Canada)
    language_isocode.addItems.fr-ch = French (Switzerland)
    language_isocode.addItems.it-ch = Italian (Switzerland)
    language_isocode.addItems.nl-be = Dutch (Belgium)
}